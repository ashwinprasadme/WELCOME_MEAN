angular.module('app', []);

// services are defined as factory
// Singleton service
// https://sourcemaking.com/design_patterns/singleton

angular.module('app').factory('messages', function(){
  // empty object
  var messages = {};

  // property of object messages
  messages.list = [];

  // function of messages
  messages.add = function(message){
    messages.list.push({
      id: messages.list.length,
      text: message});
  };

  return messages;
});

// Controller's function injects our 'messages' service
angular.module('app').controller('ListCtrl', function (messages){
  var self = this;
  // Again, this pointer is used to avoid ambiguity
  // self.messages - is in the controllers $scope
  // messages.list - is the service's list (Singleton)
  self.messages = messages.list;
});

// Similar to above Controller, uses funtion from service
angular.module('app').controller('PostCtrl',function (messages) {
  var self = this;
  // Use a default message string
  self.newMessage = 'Hello World!';

  self.addMessage = function (message) {
    messages.add(message);
    self.newMessage = ' ';
    // Clear out the text input feild after entry
  };

});
