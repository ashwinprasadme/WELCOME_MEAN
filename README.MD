# Getting started with MEAN Stack
### MongoDB Express.js AngularJS Node.js
****

**Look Into:**
+ [REST](http://en.wikipedia.org/wiki/Representational_state_transfer)
+ [Node.js](https://nodejs.org/en/)
+ [NPM](https://www.npmjs.com/)
+ [express](https://www.npmjs.com/package/express)
+ [MongoDB](https://docs.mongodb.org/getting-started/shell/introduction/)

** Finish these!**

AngularJS - https://thinkster.io/a-better-way-to-learn-angularjs


****
** Prerequisites**

+ Install


 [Node.js](https://github.com/nodejs/node-v0.x-archive/wiki/Installing-Node.js-via-package-manager) RunTime

    Setup with Ubuntu:

      sudo bash -
      curl --silent --location https://deb.nodesource.com/setup_0.12

  Then install with Ubuntu:

      sudo apt-get install --yes nodejs


+ Install [MongoDB](https://docs.mongodb.org/manual/installation/)

  With Ubuntu

      echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
  Update and install :

      sudo apt-get update
      sudo apt-get install -y mongodb-org

+ HTTP Server if you dont have aleready
    sudo apt-get install apache2


****
