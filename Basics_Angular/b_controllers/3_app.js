// register angular module, used with directive ng-app
// parameter 1: Name , parameter 2: Dependencies
angular.module('app',[]);

// register angular controller, used directly by the ng-controller
// parameter 1: Controller name , parameter 2: Implementation

angular.module('app').controller('MainCtrl',function(){
// Using 'this' to avoid confusion with variable scopes when we start nesting functions
var self = this;
self.message = 'hello';

self.change = function (msg) {
  self.message = msg;
};

});
